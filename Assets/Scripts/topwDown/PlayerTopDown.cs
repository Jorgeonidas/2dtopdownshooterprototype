﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTopDown : Ship {
	[SerializeField] private LayerMask layerMask;
	[SerializeField] private GameObject bolt;
	[SerializeField] SimpleTouchPad touchPad;
	[SerializeField] RotationFiringPad rotationFiringPad;
	private CharacterController characterController;
	//private Rigidbody rigidbody;
	private Vector3 moveDirection;
	private Vector3 currentLooktarget;
	private float fireTimer;
	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
		fireTimer = 0.0f;
		//rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		//pc firing
		if(Input.GetMouseButton(0) && Time.time > fireTimer){
			fireTimer = Time.time + FireRate;
			Instantiate(bolt,transform.position,transform.rotation);
		}
		move();
	}
	void FixedUpdate(){
		rotate();
	}

	public override void  move(){
		moveDirection =new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		characterController.SimpleMove(moveDirection*Speed);

		/*Android */
		/*Vector2 direction = touchPad.GetDirection();
		moveDirection = new Vector3(direction.x,transform.position.y,direction.y);
		characterController.SimpleMove(moveDirection*Speed);*/

	}

	public void rotate(){
		//PC
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Debug.DrawRay(ray.origin,ray.direction*500,Color.red);
		if(Physics.Raycast(ray,out hit,500,layerMask,QueryTriggerInteraction.Ignore)){
			if(hit.point != currentLooktarget){
				currentLooktarget = hit.point;
			}
			Vector3 targetPosition = new Vector3 (currentLooktarget.x, transform.position.y, currentLooktarget.z);
			//calculamos y asignamos la nueva rotacion interpolando del angulo origen al angulo final
			Quaternion targetRotation = Quaternion.LookRotation (targetPosition - transform.position);
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation,Time.deltaTime *10f);
		}

		//android
		//float angle = rotationFiringPad.getAngle();
		//transform.rotation =new Vector3(0,angle,0);
	}
}
