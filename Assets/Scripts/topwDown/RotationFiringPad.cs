﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class RotationFiringPad : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

    public float smoothing;

    private Vector2 origin;
    private Vector2 direction;
    private Vector2 smoothDirection;
    private bool touched;
    private int pointerID;
	private float angle;
    void Awake () {
        direction = Vector2.zero;
        touched = false;
    }

    public void OnPointerDown (PointerEventData data) {
        if (!touched) {
            touched = true;
            pointerID = data.pointerId;
            origin = data.position;
        }
    }

    public void OnDrag (PointerEventData data) {
        if (data.pointerId == pointerID) {
            Vector2 currentPosition = data.position;
            Vector2 direction = currentPosition - origin;
			angle = Vector2.Angle(direction, currentPosition+direction);
            //direction = directionRaw.normalized;
			//targetRotation = Quaternion.LookRotation(direction);

        }
    }

    public void OnPointerUp (PointerEventData data) {
        if (data.pointerId == pointerID) {
            direction = Vector3.zero;
            touched = false;
        }
    }

    public float getAngle () {
		return angle;
    }


}