﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyExplosionVFX : MonoBehaviour {
	[SerializeField] float time;
	[SerializeField] Vector3 destinationScale;

	void Start () {
			StartCoroutine(ScaleOverTime(time, destinationScale));
	}

	IEnumerator ScaleOverTime(float time, Vector3 targetScale)
     {
         Vector3 originalScale = transform.localScale;
         float currentTime = 0.0f;
         
         do
         {
             transform.localScale = Vector3.Lerp(originalScale, targetScale, currentTime / time);
             currentTime += Time.deltaTime;
             yield return null;
         } while (currentTime <= time);
         
         Destroy(gameObject);
     }
}
