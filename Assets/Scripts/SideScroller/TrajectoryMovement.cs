﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryMovement : MonoBehaviour {

	private int target = 0; //indice de el arreglo de checkpoints
	[SerializeField] private GameObject trajectory;
	[SerializeField] private float navigationUpdate;
	[SerializeField] private float exitSpeed;
	[SerializeField] private float rotSpeed;
	//[SerializeField] private GameObject explosionVfx;
	
	private Transform exitPoint;
	private List<Transform> waypoints;
	private Transform enemy;
	//private Collider2D enemyCollider;
	private float navigationTime = 0;
	public float speedMultiplier = 0.5f;
	private bool finisWaipoints = false;
	private SpriteRenderer sprite;
	// Use this for initialization
	void Awake () {
		exitPoint = trajectory.GetComponent<TrajectoryForTheEnemy>().ExitPoint;
		waypoints = trajectory.GetComponent<TrajectoryForTheEnemy>().WayPoints;
		enemy = GetComponent<Transform>();
		//enemyCollider = GetComponent<Collider2D>();
		sprite = GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(waypoints != null && !finisWaipoints ){
			navigationTime += Time.deltaTime;
			if(navigationTime > navigationUpdate){
				if(target < waypoints.Count){ //recorriendo los waypoints									
					enemy.position = Vector2.MoveTowards(enemy.position,waypoints[target].position , navigationTime*speedMultiplier);
					Quaternion finalRot = Quaternion.LookRotation(waypoints[target].position - enemy.position, Vector3.forward);
					transform.rotation = Quaternion.RotateTowards(transform.rotation,finalRot, rotSpeed*Time.deltaTime);
				}else{//si ya no hay mas waypoints se va al punto de salida					
					enemy.position = Vector2.MoveTowards(enemy.position, exitPoint.position, navigationTime*speedMultiplier);
				}
				
				navigationTime = 0;
			}
		}
		if(finisWaipoints){
			//print("exiting");
			GetComponent<Rigidbody2D>().velocity = enemy.forward*exitSpeed;
		}
	}
	//cada checkpoint que colisiona aumenta el index del array de checkpoints para pasar al siguiente
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "waypoint"){	
			target += 1;			
		}else if (other.tag == "finish"){
			finisWaipoints = true;

		}
		
	}
}
