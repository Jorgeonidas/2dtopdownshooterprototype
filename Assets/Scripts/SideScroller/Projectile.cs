﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	[SerializeField] float speed;
	[SerializeField] int damage;

	public int Damage{
		get{
			return damage;
		}
	}
	void FixedUpdate(){
		transform.Translate(Vector2.up*speed*Time.deltaTime);
	}

}
