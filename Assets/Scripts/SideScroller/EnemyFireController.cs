﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireController : MonoBehaviour {
	//por ahora.. luego se crearan distintos tipos de prjectiles
	[SerializeField] private GameObject projectile;
	[SerializeField] private float delay = 2f;
	[SerializeField] private Transform[] cannonsPos;
	private EnemyShip enemyShip;
	
	// Use this for initialization
	void Start () {
		enemyShip = GetComponent<EnemyShip>();
		StartCoroutine(fire());	
	}

	IEnumerator fire(){
		if(enemyShip.EnemyIsAlive){
			//play audio
			enemyShip.fireAnimation();
			for(int i = 0; i < cannonsPos.Length; i++){
				GameObject newPorjectile = Instantiate(projectile,cannonsPos[i].position,transform.rotation);
			}
			yield return new WaitForSeconds(Random.Range(delay,enemyShip.FireRate));
			StartCoroutine(fire());						
		}
		yield return null;	
	}

}
