﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
	[SerializeField] protected float speed;
	[SerializeField] protected float firerate;
	[SerializeField] protected int hitPoints;
	
	public float Speed{
		get{
			return speed;
		}
	}
	public float FireRate{
		get{
			return firerate;
		}
		set{
			firerate = value;
		}
	}
	public int HitPoints{
		get{
			return hitPoints;
		}
		set{
			hitPoints = value;
		}
	}

	public virtual void move(){
		//transform.Translate(Vector2.up*speed*Time.deltaTime);
		GetComponent<Rigidbody2D>().velocity = transform.up*Speed;
	}
	public virtual void takeDamage(int damage){
		//Debug.Log("taking damage");
		HitPoints -= damage;
		//Debug.Log(HitPoints + "inside parent function");
	}
}
