﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakPointHealth : MonoBehaviour {
	[SerializeField] private int hitPoints;

	public int HitPoints{
		get{return hitPoints;}
		set{hitPoints = value;}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "playerBolt"){
			takeDamage(other.gameObject.GetComponent<Projectile>().Damage);
			Destroy(other.gameObject);
		}
	}

	public void takeDamage(int dmg){
		HitPoints -= dmg;
		if(HitPoints <= 0){
			Destroy(gameObject);
		}
	}
}
