﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {
	private Transform target;
	[SerializeField] private int hitPoints;
	[SerializeField] private float rotationSpeed;
	[SerializeField] private float delay;
	[SerializeField] private float fireRate;
	[SerializeField] float FieldOfView = 25f;
	[SerializeField] private GameObject bolt;
	[SerializeField] private Transform turretCannon;
	[SerializeField] private GameObject explosionVfx;
	//shake camera effect 
	[SerializeField] private float shakeCamAmmount;
	[SerializeField] private float shakeTime;

	//private bool is
	private bool alingWithTarget;
	private Quaternion finalRot;
	private float fireTimer;
	private float auxRot;
	private SpriteRenderer sprite;
	private CircleCollider2D coll;
	//getters & setters
	public bool AlingWithTarget{
		get{
			return alingWithTarget;
		}
		set{
			alingWithTarget = value;
		}
	}

	public float RotationSpeed{
		get{
			return rotationSpeed;
		}
		set{
			rotationSpeed = value;
		}
	}
	public float AuxRot{
		get{
			return auxRot;
		}
		set{
			auxRot = value;
		}
	}

	public int HitPoints{
		get{
			return hitPoints;
		}
		set{
			hitPoints = value;
		}
	}

	void Awake () {
		AuxRot = RotationSpeed;
		AlingWithTarget = false;
		target = GameObject.Find("Player").GetComponent<Transform>();
		fireTimer = 0.0f;

		sprite = GetComponentInChildren<SpriteRenderer>();
		coll = GetComponent<CircleCollider2D>();
	}
	
	void Start(){
		StartCoroutine(shot());
	}

	void Update () {
		if(HitPoints > 0 && GameManager.Instance.Player.GetComponent<PlayerSidesCroller>().PlayerIsAlive){
			rotateToTarget();
		}					
	}

	//rotar al objetivo
    public void rotateToTarget(){
		finalRot = Quaternion.LookRotation(target.position - transform.position,Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation,finalRot,Time.deltaTime*rotationSpeed);
	}

	//retraso de la torreta luego de alinearse
	IEnumerator turretDelay(float auxR){
		RotationSpeed = 0;

		yield return new WaitForSeconds(delay);

		RotationSpeed = auxR;
		AlingWithTarget = false;	
	}

	//dispara
	IEnumerator shot(){
		AlingWithTarget = InFOV();
		if(AlingWithTarget){
			Instantiate(bolt, turretCannon.position,turretCannon.rotation);
			yield return new WaitForSeconds(fireRate);
			StartCoroutine(shot());
		}else{
			yield return new WaitForSeconds(delay);
			StartCoroutine(shot());
		}
	}

	//collisions
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "playerBolt"){
			takeDamage(other.gameObject.GetComponent<Projectile>().Damage);
			Destroy(other.gameObject);
		}
	}

    public void takeDamage(int damage)
    {
        HitPoints -= damage;
		if(HitPoints <= 0){
			StartCoroutine(enemyDeath());
		}
    }

  	 IEnumerator enemyDeath()
    {
		CameraShake.Instance.Shake(shakeCamAmmount,shakeTime);
		GameManager.Instance.SpawnItem(transform.position);
		Instantiate(explosionVfx,transform.position, Quaternion.identity);
		sprite.enabled = false;
		coll.enabled = false;
		yield return new WaitForSeconds(1f);
	   	Destroy(gameObject);
    }

	public bool InFOV(){
		Vector3 DirToTarget = target.position - turretCannon.position;

		//Get angle between forward and look direction
		float Angle = Vector3.Angle(turretCannon.up, DirToTarget);

		//Are we within field of view?
		if(Angle <= FieldOfView)
			return true;

		//Not within view
		return false;
	}
}
