﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLevel1 : LevelBoss {
	[SerializeField] private Vector2 xLeftLimit;
	[SerializeField] private Vector2 xRightLimit;
	[SerializeField] private Vector2 xCenter;
	//[SerializeField] private Vector2 yLimits;

	public override IEnumerator StartRoutine(){
		yield return new WaitForSeconds(TimeToStartCombat);
		//activo los primeros puntos debiles
		ActivatePhaseWeakPoints(0,1);
		yield return new WaitForSeconds(1f);
		//pasamos a la fase de combate
		BossState = BOSS_STATE.COMBAT_BEHAVIOUR;
	}

	//aca va el combate
	public override IEnumerator CombatRoutine(){
		while(BossState == BOSS_STATE.COMBAT_BEHAVIOUR){
			if(WeakPoints[0] != null && WeakPoints[1] != null){
				Vector2 pos = new Vector2(transform.position.x, transform.position.y);
				yield return StartCoroutine(xMove(transform, pos,xLeftLimit, 3));
				yield return StartCoroutine(xMove(transform, xLeftLimit,xCenter, 3));
				yield return StartCoroutine(xMove(transform, xCenter, xRightLimit, 3));
			}else if((WeakPoints[2] != null && WeakPoints[3] != null) && (WeakPoints[0] == null && WeakPoints[1] == null)){
				print("fase 2");
				//corrutina de ataque 2
				ActivatePhaseWeakPoints(2,3);
				Vector2 pos = new Vector2(transform.position.x, transform.position.y);
				yield return StartCoroutine(xMove(transform, pos,xLeftLimit, 3));
				yield return StartCoroutine(xMove(transform, xLeftLimit,xCenter, 3));
				yield return StartCoroutine(xMove(transform, xCenter, xRightLimit, 3));
			}else if((WeakPoints[2] == null && WeakPoints[3] == null) && (WeakPoints[4]!=null) ){
				//corrutina ataque 3
				print("fase 3");
				ActivatePhaseWeakPoints(4,4);		
			}else if(WeakPoints[4] == null){
				BossState = BOSS_STATE.DEAHT;
			}
			yield return null;
		}
		yield return null;
	}

    IEnumerator xMove(Transform thisTransform, Vector2 startPos, Vector2 endPos, float time)
    {
		//COMO SABREMOS LOS PUNTOS DEBILES EN ESA FASE? UNA LISTA?
		if(WeakPoints[0] != null || WeakPoints[1] != null){	
			var i= 0.0f;
			var rate= 1.0f/time;
			while(i < 1.0f)
			{
				i += Time.deltaTime * rate;
				thisTransform.position = Vector2.Lerp(startPos, endPos, i);
				yield return null;
			}
			float randTime = UnityEngine.Random.Range(1,2);
			yield return new WaitForSeconds(randTime);
		}else
			yield return null;
    }

    public override IEnumerator DeahtRoutine(){
		BossIsAlive = false;
		GameManager.Instance.BossDefeated = true;
		yield return new WaitForSeconds(3f);
		Destroy(gameObject);
	}


}
