﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum MoveFunction{
	sin, cos, down, circle, kamikaze, debugMode
};


public class EnemyShip : Ship {
	[SerializeField] private MoveFunction moveFunction;
	[SerializeField] private float xSpeed;
	[SerializeField] private float amplitud = 2;
	[SerializeField] private float smoothing = 2;
	[SerializeField] private Boundary boundary;
	[SerializeField] private int collisionDamage;
	[SerializeField] private GameObject explosionVfx;
	//shake camera effect 
	[SerializeField] private float shakeCamAmmount;
	[SerializeField] private float shakeTime;
	
	private float currentSpeed;
	private float targetXmove;
	private Collider2D coll;
	private SpriteRenderer sprite;
	private Animator enemyAnim;
	private bool enemyIsAlive;

	private Vector2 startPos;
	private Vector3 prevLoc;
	private Transform[] GoChildrens;

	private float timer = 0f;
	// Use this for initialization
	public MoveFunction MoveFunction{
		get{
			return moveFunction;
		}
	}

	public int CollisionDamage{
		get{
			return collisionDamage;
		}
	}

	public bool EnemyIsAlive{
		get{
			return enemyIsAlive;
		}
		set{
			enemyIsAlive = value;
		}
	}

	void Awake () {
		startPos = transform.position;
		coll = GetComponent<Collider2D>();
		sprite = GetComponentInChildren<SpriteRenderer>();
		enemyAnim = GetComponentInChildren<Animator>();
		coll.enabled = true;
		sprite.enabled = true;
		EnemyIsAlive = true;
		prevLoc = transform.position;
		GoChildrens = GetComponentsInChildren<Transform>();
	}
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(MoveFunction != MoveFunction.debugMode){			
			move();
			rollAnimation();
		}
			
		 
	}

	public override void  move(){
		
		base.move();
		if(moveFunction == MoveFunction.sin)
			xSinMove();
		if(moveFunction == MoveFunction.cos)
			xCosMove();
		if(moveFunction == MoveFunction.kamikaze){
			Transform playerPos = GameManager.Instance.Player.transform;
			float velPerc = 5;
			if (GameManager.Instance.Player.gameObject.GetComponent<PlayerSidesCroller>().PlayerIsAlive) {
				Vector2 diferencia = transform.position - playerPos.position;
				if (diferencia.y < 3 && diferencia.y > -2 ) {//distancia solo en el eje y de ambos objetos
					//trata de ponerse a la misma posicion y del player
					transform.position = Vector2.MoveTowards (transform.position, playerPos.position, velPerc * Time.deltaTime);
				}
			}
		}
		GetComponent<Rigidbody2D>().position = new Vector2
		(
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.x, boundary.xMin, boundary.xMax), 
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.y, boundary.yMin, boundary.yMax)			
		);
	}

    public void xCosMove()
    {
       	Vector2 currentPos = startPos;
		currentPos.x += amplitud*Mathf.Cos(Time.time*xSpeed);
		currentPos.y = transform.position.y;
		transform.position = currentPos;
    }

    public void xSinMove(){
		Vector2 currentPos = startPos;
		currentPos.x += amplitud*Mathf.Sin(Time.time*xSpeed);
		currentPos.y = transform.position.y;
		transform.position = currentPos;
	}
	
	public void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "playerBolt"){
			takeDamage(other.gameObject.GetComponent<Projectile>().Damage);
			Destroy(other.gameObject);
		}
		if(other.tag == "Player"){
			//print("just collide the player!");
			GameManager.Instance.Player.GetComponent<PlayerSidesCroller>().takeDamage(CollisionDamage);
			StartCoroutine(enemyDeath());
		}
	}

	public override void takeDamage(int damage){
		base.takeDamage(damage);
		if(HitPoints <= 0){
			StartCoroutine(enemyDeath());
		}
	}

    IEnumerator enemyDeath()
    {
		CameraShake.Instance.Shake(shakeCamAmmount,shakeTime);
		GameManager.Instance.SpawnItem(transform.position);
		EnemyIsAlive = false;
		Instantiate(explosionVfx,transform.position,Quaternion.identity);
		//sprite.enabled = false;
		for(int i = 0; i < GoChildrens.Length; i++){
			GoChildrens[i].gameObject.SetActive(false);
		}
		coll.enabled = false;
		yield return new WaitForSeconds(0.5f);
	   	Destroy(gameObject);
    }

	//animaciones
	public void fireAnimation(){
		if(enemyAnim != null){
			print("ShotAnim");
			enemyAnim.SetTrigger("Attack");
		}
	}

	public void rollAnimation(){
		Vector2 currentVel = (transform.position - prevLoc)/Time.deltaTime;
		enemyAnim.SetFloat("Alebeo",currentVel.x);
		prevLoc = transform.position;
	}

}
