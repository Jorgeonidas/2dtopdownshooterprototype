﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {
	[SerializeField] private float shieldDuration = 5f;
	//[SerializeField] SpriteRenderer sprite;
	[SerializeField] private LayerMask layer;
	// Use this for initialization

	void OnEnable(){
		//print("habilitando objeto");
		StartCoroutine(disableShield(shieldDuration));
	}

	void OnDisable(){
		print("desabilitado");
		GameManager.Instance.Player.GetComponent<PlayerSidesCroller>().IsShieldActive = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(gameObject.tag == "playerShield"){
			if(other.tag == "Enemy")
			{
				other.gameObject.GetComponent<EnemyShip>().takeDamage(100);

			}else if(other.tag != "waypoint" || other.tag == "Item" ){
				Destroy(other.gameObject);
			}

		}
	}

	IEnumerator disableShield(float time){
		yield return new WaitForSeconds(time);
		this.gameObject.SetActive(false);
	}
}
