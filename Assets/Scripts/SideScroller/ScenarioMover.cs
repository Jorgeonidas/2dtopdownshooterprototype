﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioMover : MonoBehaviour {
	public enum TILE_TYPE {ENDLESS,CONTINUE};

	[SerializeField] TILE_TYPE tileType;
	[SerializeField] private Vector2 limits;
	[SerializeField] private float scenarioSpeed = 2f;	
	
	// Update is called once per frame
	void Update () {
		if(GameManager.Instance.Player.GetComponent<PlayerSidesCroller>().PlayerIsAlive){
			transform.Translate(Vector3.up*-scenarioSpeed*Time.deltaTime);
			if(transform.position.y <= limits.x){
				GameManager.Instance.substractTile();
				if(tileType == TILE_TYPE.ENDLESS){
					transform.position = new Vector3(0,limits.y,0);
				}else{
					Destroy(gameObject);
				}		
			}
		}
	}

}
