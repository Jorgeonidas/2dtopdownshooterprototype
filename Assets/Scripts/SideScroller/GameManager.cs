﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;

public class GameManager : Singleton<GameManager> {

	public enum GAME_STATE {WAVE, BOSS_FIGHT, WIN, LOSE};

	//states of the game
	[SerializeField] GAME_STATE currentState;
	//-----
	[SerializeField] private int tilesToBossFight;
	[SerializeField] private bool bossDefeated;
	//--
	[SerializeField] private GameObject player;
	//wavemode
	[SerializeField] private int totalWaves;
	[SerializeField] private int currentWave;
	//n horde mode
	[SerializeField] private int enemiesPerWave;
	[SerializeField] private Transform[] spawnPoints;
	//[SerializeField] private Transform traySpawnPoint;
	//enemies 
	[SerializeField] private GameObject[] enemiesHazzards;
	[SerializeField] private GameObject[] trayEnemies;
	[SerializeField] private GameObject LevelBoss;
	[SerializeField] private float timeBetweenSpawn;
	[SerializeField] private bool debugMode;
	//itmes to spawn
	[SerializeField] private GameObject[] powerUps;
	[SerializeField] private int countToSpawnPW;
	//Scenario-BossScenario
	private int currentTileIndex;

	
	private int ctsAux;
	private Scene currentScene;

	public GameObject Player{
		get{
			return player;
		}
	}

	public int EnemiesPerWave{
		get{
			return enemiesPerWave;
		}
		set{
			enemiesPerWave = value;
		}
	}

	public float TimeBetweenSpawn{
		get{
			return timeBetweenSpawn;
		}
	}
	//maquina de estados del juego
	public GAME_STATE CurrentState{
		get{
			return currentState;
		}
		//aca asignaremos y cambiaremos el estado
		set{
			currentState = value;

			StopAllCoroutines();

			switch (currentState)
			{
				case GAME_STATE.WAVE:
					StartCoroutine(wave());
				break;

				case GAME_STATE.BOSS_FIGHT:
					print("BOSS FIGHT");
					StartCoroutine(bossFight());
				break;

				case GAME_STATE.WIN:
					print("MISSION COMPLETE");
				break;
				
				case GAME_STATE.LOSE:
					print("YOU LOSE");
					StartCoroutine(loadMain());
				break;
			}

		}
	}

	public int CurrentTileIndex{
		get{
			return currentTileIndex;
		}
		set{
			currentTileIndex = value;
		}
	}

	public bool BossDefeated{
		get{
			return bossDefeated;
		}
		set{
			bossDefeated = value;
		}
	}

	void Awake(){
		BossDefeated = false;
	}

	void Start () {
		currentScene = SceneManager.GetActiveScene();
		Assert.IsNotNull(Player);

		if(currentScene.name == "MainMenuScene")
			return;
		 
		CurrentState = GAME_STATE.WAVE;
			
		//variable auxiliar para saber cada cuantos enemigos se instancia un powerup
		ctsAux = countToSpawnPW;

	}
	//--estados--//
	//ESTADO WAVE
	IEnumerator wave(){
		while(CurrentState == GAME_STATE.WAVE){
			if(Player.GetComponent<PlayerSidesCroller>().PlayerIsAlive){
				//no he llegado al jefe
				if(tilesToBossFight > 0){
						if(!debugMode){
							GameObject newEnemy;
							Transform spawnPoint = spawnPoints[Random.Range(0,spawnPoints.Length)];
							if(spawnPoint.gameObject.tag == "TraySpawner"){
								//print("formation");
								newEnemy = trayEnemies[Random.Range(0,trayEnemies.Length)];
								Instantiate(newEnemy,spawnPoint.position,Quaternion.identity);
								yield return new WaitForSeconds(timeBetweenSpawn);
							}else{
								//print("enemy");
								newEnemy = enemiesHazzards[Random.Range(0,enemiesHazzards.Length)];
								Instantiate(newEnemy,spawnPoint.position,spawnPoint.rotation);
							}	
						}				
						yield return new WaitForSeconds(TimeBetweenSpawn);
				}else{//pelear contra el jefe
					//insntanciar boos
					Instantiate(LevelBoss,transform.position,Quaternion.identity);
					CurrentState = GAME_STATE.BOSS_FIGHT;
				}
			//estoy muerto
			}else{
				CurrentState = GAME_STATE.LOSE;
			}
		}
	}

	//pelea contra el jefe
	IEnumerator bossFight(){
		while(currentState == GAME_STATE.BOSS_FIGHT){
			if(BossDefeated){
				CurrentState = GAME_STATE.WIN;
			}
			yield return null;
		}
	}

	//ESTADO LOSE
	IEnumerator loadMain(){
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene("MainMenuScene");
	}

	//---funciones--//
	//spawn item
	public void SpawnItem(Vector3 position){
		countToSpawnPW -= 1;
		if(countToSpawnPW <= 0){
			GameObject newPowerUp = powerUps[Random.Range(0,spawnPoints.Length)];
			Instantiate(newPowerUp, position, Quaternion.identity); 
			countToSpawnPW = ctsAux;
		}
	}

	//cada vez que un plano pasa estoy mas cerca de pelear contra el boss
	public void substractTile(){
		tilesToBossFight -= 1;
	}

}
