﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*CLASE PADRE PARA TODOS LOS BOSSES DEL JUEGO */
/*CADA BOSS SOBREESCRIBIRA SU COMPORTAMIENTO */
public class LevelBoss : MonoBehaviour {
	public enum BOSS_STATE{
		START, COMBAT_BEHAVIOUR, DEAHT
	}
	[SerializeField] BOSS_STATE bossState;
	[SerializeField] int numberOfPhases = 2;
	[SerializeField] int currentPhase;
	[SerializeField] GameObject[] weakPoints;
	[SerializeField] private bool bossIsAlive; 
	[SerializeField] private float timeToStartCombat;
	// Use this for initialization

	//maquina de estados del juego
	public BOSS_STATE BossState{
		get{
			return bossState;
		}
		//aca asignaremos y cambiaremos el estado
		set{
			bossState = value;

			StopAllCoroutines();

			switch (bossState)
			{
				case BOSS_STATE.START:
					StartCoroutine(StartRoutine());
				break;

				case BOSS_STATE.COMBAT_BEHAVIOUR:
					print("BOSS COMBAT PHASE");
					StartCoroutine(CombatRoutine());
				break;

				case BOSS_STATE.DEAHT:
					StartCoroutine(DeahtRoutine());
				break;

			}

		}
	}
	//esta vivo?
	public bool BossIsAlive{
		get{return bossIsAlive;}
		set{bossIsAlive = value;}
	}
	//puntos debiles
	public GameObject[] WeakPoints{
		get{return weakPoints;}
	}
	//numero de fases
	public int NumberOfPhases{
		get{return numberOfPhases;}
		set{numberOfPhases = value;}
	}
	//fase actual
	public int CurrentPhase{
		get{return currentPhase;}
		set{currentPhase = value;}
	}
	//
	public float TimeToStartCombat{
		get{return timeToStartCombat;}
		set{timeToStartCombat = value;}
	}

	//Comenzando el combate
	public virtual void Start () {
		BossIsAlive = true;
		currentPhase = 0;
		//desactivo los puntos debiles
		for (int i = 0; i < WeakPoints.Length; i++)
		{
			WeakPoints[i].SetActive(false);
		}
		//voy a la face inicial por lo general ejecutara una animacion de presentacion
		BossState = BOSS_STATE.START;
	}
	
	//
	public virtual IEnumerator StartRoutine(){
		
		yield return null;
	}

	public virtual IEnumerator CombatRoutine(){
		
		yield return null;
	}

	public virtual IEnumerator DeahtRoutine(){
		
		yield return null;
	}
	//0 to n-1
	public void ActivatePhaseWeakPoints(int from,int to){
		for(int i = from; i <= to; i++ ){
			WeakPoints[i].SetActive(true);
		}
	}

}
