﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryForTheEnemy : MonoBehaviour {
	[SerializeField] private Transform startPoint;
	[SerializeField] private Transform exitPoint;
	[SerializeField] private List<Transform> wayPoints;
	[SerializeField] private GameObject enemy;
	[SerializeField] private int enemiesToSpawn;
	[SerializeField] private float timeBetweenSpawn;

	private int enemiesSpawned;
	//list of enemies in the path
	private List<TrajectoryMovement> enemiesInPath;

	public Transform ExitPoint{
		get{
			return exitPoint;
		}
	}

	public List<Transform> WayPoints{
		get{
			return wayPoints;
		}
	}

	public int EnemiesSpawned{
		get{
			return enemiesSpawned;
		}
		set{
			enemiesSpawned = value;
		}
	}

	void Awake(){
		
		//lista de enemigos
		enemiesInPath = new List<TrajectoryMovement>();
		enemiesInPath.Clear();
		
	}

	void Start(){
		StartCoroutine(spawn());
		//buscar por que no esta contando bien la lista ya que esta es la solucion pirata
		Destroy(gameObject, 12f);
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void destroyTrajectoryGo()
    {

    }

    IEnumerator spawn(){
		while(EnemiesSpawned < enemiesToSpawn){
			EnemiesSpawned++;
			Instantiate(enemy,startPoint.position,startPoint.rotation);
			yield return new WaitForSeconds(timeBetweenSpawn);
		}	
	}

}
