﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum MovemetType{
	sideScroller, topDown
};
public class PlayerSidesCroller : Ship {
	[SerializeField] private Boundary boundary;
	[SerializeField] private Projectile projectile;
	[SerializeField] private int maxHitPoins;
	[SerializeField] private MovemetType movemetType;
	[SerializeField] private GameObject DeathVfx;
	[SerializeField] private bool isShieldActive;
	[SerializeField] private GameObject shield;

	private float firetimer;
	private Rigidbody2D playerRb;
	private float horizontal;
	private float vertical;
	private Animator anim;
	private Vector3 prevLoc;
	//death variables
	private bool playerIsAlive;
	private SpriteRenderer playerSprite;
	private BoxCollider2D playerCol;


	//getters & setters
	public MovemetType MovemetType{
		get{
			return movemetType;
		}
	}

	public bool PlayerIsAlive{
		get{
			return playerIsAlive;
		}
		set{
			playerIsAlive = value;
		}
	}

    public bool IsShieldActive{
		get{
			return isShieldActive;
		}
		set{
			isShieldActive = value;
		}
	}
	
	// Use this for initialization
	void Start () {
		playerRb = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		firetimer = 0.0f;
		prevLoc =transform.position;//posicion inicial
		playerSprite = GetComponentInChildren<SpriteRenderer>();
		playerCol = GetComponent<BoxCollider2D>();
		playerSprite.enabled = true;
		playerCol.enabled = true;
		PlayerIsAlive = true;
		IsShieldActive = false;
	}

	void FixedUpdate(){
		if(PlayerIsAlive){
			move();
			//roll();
		}
		
	}
    // Update is called once per frame
    void Update () {
		if(PlayerIsAlive){
			if(Input.GetMouseButton(0) && Time.time > firetimer){
				firetimer = Time.time + FireRate;
				shot();
			}
		}
	}
	//functions
	public override void  move(){
	
		#if UNITY_STANDALONE || UNITY_WEBPLAYER
		horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");				
		playerRb.velocity = new Vector2(horizontal*Speed,vertical*Speed);
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		if(Input.touchCount > 0){
			Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position); 
			touchPos.z = 0;
			touchPos.y += 1;
			if(Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Stationary){
				transform.position = Vector3.MoveTowards(transform.position, touchPos, Speed*Time.deltaTime);
			}else if(Input.GetTouch(0).phase == TouchPhase.Moved){
				transform.position = touchPos;
			}
		}
		#endif
		//limites de la pantalla
		GetComponent<Rigidbody2D>().position = new Vector2
		(
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.x, boundary.xMin, boundary.xMax), 
			Mathf.Clamp (GetComponent<Rigidbody2D>().position.y, boundary.yMin, boundary.yMax)			
		);
				 		
	}

	public void roll(){
		#if UNITY_STANDALONE || UNITY_WEBPLAYER
		if(Input.GetAxis("Horizontal") > 0.1f){
			anim.Play("rotarDer");
		}else if(Input.GetAxis("Horizontal") < -0.1f){
			anim.Play("rotarIzq");
		}else{
			anim.Play("Idle");
		}
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		//detectar la direccion a la que se mueve para hacerlo rotar
		Vector2 currentVel = (transform.position - prevLoc)/Time.deltaTime;
		if(currentVel.x > 0.1){
			anim.Play("rotarDer");
		}else if(currentVel.x < -0.1){
			anim.Play("rotarIzq");
		}else{
			anim.Play("Idle");
		}
		prevLoc = transform.position;
		#endif
	}

    public void shot()
    {
		Instantiate(projectile,transform.localPosition,transform.localRotation);
    }
	

	public override void takeDamage(int damage){
		//alguna animacion por el estilo
		HitPoints -= damage;
		print(HitPoints);
		if(HitPoints <= 0){
			death();
		}
	}

	//Collisions
	public void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "enemyBolt" && !IsShieldActive){
			takeDamage(other.gameObject.GetComponent<Projectile>().Damage);
			Destroy(other.gameObject);
		}
	}

	public void death(){
		PlayerIsAlive = false;
		//aca me muero!
		playerCol.enabled = false;
		playerSprite.enabled = false;
		Instantiate(DeathVfx,transform.position,transform.rotation);
	}
	//powerUps effects
	public void ActivateShield(){
		IsShieldActive = true;
		shield.SetActive(true);
	}

	public void increaseFireRate(float seconds){
		if(!((FireRate - seconds ) < 0.2f))
			FireRate -= seconds;
	}

	public void recoverHitPoints(int hpRecovered){
		if(hpRecovered + HitPoints <= maxHitPoins)
			HitPoints += hpRecovered;
		else
			HitPoints += maxHitPoins - HitPoints;
	}
}
