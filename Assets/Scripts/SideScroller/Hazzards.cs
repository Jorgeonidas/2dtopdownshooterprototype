﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazzards : MonoBehaviour {
	[SerializeField] protected float Speed;
	[SerializeField] protected int hitPoints;
	[SerializeField] protected int damage;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		Move();
	}

	public void Move(){
		transform.Translate(Vector2.up*Speed*Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){
			GameManager.Instance.Player.GetComponent<PlayerSidesCroller>().takeDamage(damage);
			//hcaer animacion de explosion
			Destroy(gameObject);
		}
		if(other.tag == "playerBolt"){
			Destroy(gameObject);
		}
	}
}
