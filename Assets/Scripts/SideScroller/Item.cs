﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum PowerUpType{
	fireIncrease, Shield, hitPointsRecovery
};
public class Item : MonoBehaviour {
	[SerializeField] private PowerUpType powerUpType;
	[SerializeField] private float fireRateIncrease;
	[SerializeField] private int hitPoinsToRecover;

	public PowerUpType PowerUpType{
		get{
			return powerUpType;
		}
	}
	
	public void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){
			Debug.Log("touchPlayer");
			PlayerSidesCroller player = other.gameObject.GetComponent<PlayerSidesCroller>();
			switch (PowerUpType)
			{
				case PowerUpType.fireIncrease:
					player.increaseFireRate(fireRateIncrease);
				break;

				case PowerUpType.hitPointsRecovery:
					player.recoverHitPoints(hitPoinsToRecover);
				break;

				case PowerUpType.Shield:
					player.ActivateShield();
				break;	
			
			}
			Destroy(gameObject);
		}
	}
}
