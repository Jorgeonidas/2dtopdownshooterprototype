﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : Singleton<CameraShake> {
	
	[SerializeField] private Camera mainCam;

	private float shakeAmmount = 0f;

	void Awake(){
		if(mainCam == null){
			mainCam = Camera.main;
		}
	} 

	/* */

	public void Shake(float ammount, float lenght)
	{
		shakeAmmount = ammount;
		//repito el temblor
		InvokeRepeating("BeginShake",0, 0.01f);
		//detengo el temblor cuando pase el tiempo (lenght);
		Invoke("StopShake",lenght);
	}

	void BeginShake(){
		if(shakeAmmount > 0 ){
			Vector3 camPos = mainCam.transform.position;
			//valores random entre 0 y 1 escalados
			float offsetX = Random.value * shakeAmmount * 2 -shakeAmmount;
			float offsetY = Random.value * shakeAmmount * 2 -shakeAmmount;
			
			camPos.x += offsetX;
			camPos.y += offsetY;

			mainCam.transform.position = camPos;
		}
	}

	void StopShake(){
		//cancelamos el temblor
		CancelInvoke("BeginShake");
		//recolocamos la camara, zero porque esta dentro del gameobject que va a temblar
		mainCam.transform.localPosition = Vector3.zero;
	}
}
